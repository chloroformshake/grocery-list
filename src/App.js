import { useState } from 'react'
import { FaRegSadCry } from 'react-icons/fa'
import './App.css'

import Header from "./components/Header"
//import FormFields from "./components/FormFields"
//import ItemTable from "./components/ItemTable"
import GrandTotal from "./components/GrandTotal"
import ItemRows from "./components/ItemRows"
import AddRows from './components/AddRows'

function App() {

  const [itemsList, setItemsList] = useState([
    {
      id: 0,
      item: "anything",
      price: "18"
    },
    {
      id: 1,
      item: "Kalimba",
      price: "2500"
    },
    {
      id: 2,
      item: "500₹ Note",
      price: "500"
    }
  ])

  const addRow = (object) => {
    const id = Math.floor(Math.random() * 10000) + 1
    const newObject = { id, ...object }
    setItemsList([...itemsList, newObject]);
  }

  const deleteRow = (id) => {
    setItemsList(itemsList.filter((itemsList) => itemsList.id !== id))
    console.log(itemsList);
  }

  const editRow = (event) => {
    console.log(event.currentTarget.parentElement.previousSibling.previousSibling.previousSibling);
    console.log(event.currentTarget.parentElement.previousSibling.previousSibling);
  }

  return (
    <div className="container">
      <Header />
      <AddRows onAdd={addRow} />
      {itemsList.length > 0 ? (<ItemRows itemsList={itemsList} onEdit={editRow} onDelete={deleteRow} />) : <p style={{ display: 'flex', flexDirection: 'column-reverse', fontSize: '12px' }}>Your List is Empty and Sad<br /><FaRegSadCry style={{ alignSelf: 'center' }} size={15} /></p>}
      <GrandTotal />
    </div>
  )
}

export default App
