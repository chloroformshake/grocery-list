import { useState } from 'react'

const AddRows = ({ onAdd }) => {
    const [item, setItemValue] = useState('')
    const [price, setPriceValue] = useState('')

    const onSubmit = (e) => {
        e.preventDefault()

        // if (!itemValue) {
        //   alert('Please add a task')
        //   return
        // }

        onAdd({ item, price })

        setItemValue('')
        setPriceValue('')
    }

    return (
        <form id="form" onSubmit={onSubmit}>

            <label>ITEM:&nbsp;
                    <input type="text" id="items-field" name="item" size="15" value={item} onChange={(e) => setItemValue(e.target.value)} placeholder="Type Here..." style={{ border: 'none', borderBottom: '1px dashed darkslategray', background: 'transparent' }} required />
            </label>

            &nbsp;&nbsp;&nbsp;
            <label>PRICE:&nbsp;
                    <input type="text" id="price-field" name="price" size="5" value={price} onChange={(e) => setPriceValue(e.target.value)} placeholder=" ₹ ?" style={{ border: 'none', borderBottom: '1px dashed darkslategray', background: 'transparent' }} required />
            </label>

            &nbsp;&nbsp;&nbsp;
            <input type="submit" id="add-btn" value='ADD' style={{ background: 'whitesmoke', border: '1px solid darkslategray', cursor: 'pointer', boxShadow: '2px 1px 0px 0px lightgray', width: '50px' }} />
            <br /><br />

        </form>



        // <form className='add-form' onSubmit={onSubmit}>
        //     <div className='form-control'>
        //         <label>Task</label>
        //         <input
        //             type='text'
        //             placeholder='Add Task'
        //             value={item}
        //             onChange={(e) => setItemValue(e.target.value)}
        //         />
        //     </div>
        //     <div className='form-control'>
        //         <label>Day & Time</label>
        //         <input
        //             type='text'
        //             placeholder='Add Day & Time'
        //             value={price}
        //             onChange={(e) => setPriceValue(e.target.value)}
        //         />
        //     </div>
        //     <input type='submit' value='Save Task' className='btn btn-block' />
        // </form>
    )
}

export default AddRows
