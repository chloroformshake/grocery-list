import { FaToilet, FaPenNib, FaThumbsUp } from 'react-icons/fa'
import UnitsAndTotal from './UnitsAndTotal'
import './style/Rows.css'

const Row = ({ row, onDelete, onEdit }) => {

    return <div className="row" key={row.id}><div className='col' id='item'>{row.item.toUpperCase()}</div><div className='col' id='price'>{row.price}</div><div className='col'><UnitsAndTotal price={row.price} /></div><div className='col' style={{ display: 'flex', justifyContent: 'center' }} ><FaPenNib style={{ cursor: 'pointer', margin: '5px', textAlign: 'center', alignSelf: 'center' }} onClick={(event) => onEdit(event)} /><FaToilet style={{ cursor: 'pointer', margin: '5px', textAlign: 'center', alignSelf: 'center' }} onClick={() => onDelete(row.id)} /></div></div>
}

export default Row
