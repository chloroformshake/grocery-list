import { FaRegHandLizard, FaRegPlusSquare } from 'react-icons/fa'
import React from "react";
import TotalOutput from "./TotalOutput";
import './style/UnitsAndTotal.css'

class UnitsAndTotal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            units: 1
        }
    }
    incrementCount = () => {
        this.setState({
            units: this.state.units + 1
        });
    }
    decrementCount = () => {
        this.setState({
            units: this.state.units > 1 ? this.state.units - 1 : 1
        });
    }
    handleChange = (event) => {
        this.setState({
            units: event.target.value
        })
    }
    render() {
        return (
            <div style={{ display: "flex", justifyContent: "space-between", alignItems: 'center' }}>
                <div className="counter">

                    <FaRegHandLizard onClick={this.decrementCount} size={13} />
                    &nbsp;
                    <input type='number' min='1' value={this.state.units} size='2' onChange={this.handleChange} />
                    <FaRegPlusSquare onClick={this.incrementCount} size={13} />
                </div>
                <TotalOutput units={this.state.units} price={this.props.price} />
            </div>
        );
    }
}

export default UnitsAndTotal