import Row from './Row'
import './style/ItemRows.css'
// import store from 'store'

const ItemsList = ({ itemsList, onDelete, onEdit }) => {
    return (
        <>
            <div className="column-heads"><div className='col' style={{ fontWeight: '600' }}>ITEM(s)</div><div className='col' style={{ fontWeight: '600' }}>PRICE</div><div className='col' style={{ display: 'flex', justifyContent: "space-between" }}><span style={{ fontWeight: '600' }}>UNIT(s)</span><span style={{ fontWeight: '600' }}>TOTAL</span></div><div className='col' style={{ fontWeight: '600' }}>OPTIONS</div></div>
            {itemsList.map((row) => (
                <Row key={row.id} row={row} onDelete={onDelete} onEdit={onEdit} />
            ))}
        </>
    )
}

export default ItemsList

// export default class ItemRows extends React.Component {
//     // constructor(props) {
//     //     super(props)
//     // }
//     render() {
//         console.log(store.get(this.props));
//         return <>dick</>
//     }
// }


// const ItemTable = () => {

//     return (
//         <>
//             <table id='items'>
//                 <thead>
//                     <tr>
//                         <th className="column-head">ITEM(s)</th>
//                         <th className="column-head">PRICE</th>
//                         <th className="column-head">UNIT(s)</th>
//                         <th className="column-head">TOTAL</th>
//                         <th id="options-column">OPTIONS</th>
//                     </tr>
//                 </thead>
//                 <tbody>
//                     <tr>
//                     </tr>
//                 </tbody>
//             </table>
//         </>
//     )
// }
